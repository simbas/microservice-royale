Microservice Royale
===================

We will compare the same services in 6 different webservers:
* [ ] go + net/http
* [ ] nodejs
* [ ] nodejs + express
* [ ] java 
* [ ] java + springboot
* [ ] scala + vert.x


The server must be able to:
* [ ] serve a /healthcheck
* [ ] create a process and return the result of this process
* [ ] convert an image from jpg to png


The metrics must:

* provide : 
  * [ ] a way to limit cpu for containers
  * [ ] a one command line test for all listed containers
* returns : 
  * [ ] a json with following metrics
  * [ ] the server startup time
  * [ ] time between image startup command to an orchestrator and server up