package msroyale;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server {

	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(9000), 0);
		server.createContext("/healthcheck", new MyHandler());
		server.start();
	}

	static class MyHandler implements HttpHandler {
		@Override
		public void handle(HttpExchange t) throws IOException {
			String response = "{ \"State\": \"OK\" }";
			t.sendResponseHeaders(200, response.length());
			t.getResponseHeaders().add("Content-Type", "application/json");

			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}