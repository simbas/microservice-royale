package main

import (
	"context"
	"fmt"
	"os"
	"io/ioutil"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/api/types/container"

	"github.com/jhoonb/archivex"
)

func main() {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	resp, err := cli.ContainerCreate(ctx, &container.Config{Image: "ss"}, &container.HostConfig{Resources: container.Resources{NanoCPUs: 500000000, }}, nil, "testfromapi")
	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

}

func doJobWithContainers(cli *client.Client) {
	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}
	for _, container := range containers {
		fmt.Printf("%s %s\n", container.ID[:10], container.Image)
		stats, err := cli.ContainerStats(context.Background(), container.ID, false)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", stats.Body)
		inspect, err := cli.ContainerInspect(context.Background(), container.ID)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", inspect.Created)

	}
}
func buildImage(cli *client.Client, path string) {
	theTar := fmt.Sprintf("%s/temp.tar", path)
	tarr := new(archivex.TarFile)
	tarr.Create(theTar)
	tarr.AddAll(path, false)
	tarr.Close()
	dockerBuildContext, err := os.Open(theTar)
	defer dockerBuildContext.Close()
	options := types.ImageBuildOptions{
		SuppressOutput: false,
		Remove:         true,
		ForceRemove:    true,
		PullParent:     true}
	buildResponse, err := cli.ImageBuild(context.Background(), dockerBuildContext, options)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	fmt.Printf("********* %s **********", buildResponse.OSType)
	response, err := ioutil.ReadAll(buildResponse.Body)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	fmt.Println(string(response))
}
